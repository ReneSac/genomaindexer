#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  Serves as a configuration file and dummy object storing the information
# needed by the program.
#
#  Copyright (c) 2012 René du R. Sacramento
#  Released under the MIT license.

import os
from ConfigParser import SafeConfigParser
import codecs




class InfoDir(object):
    '''
    Path to the directories used by the program.
    '''
    def __init__(self, database_dir=None, galaxy_dir=None):
        self.program = os.path.dirname(__file__)
        self.plugins = [os.path.join(self.program, "plugins/")]
        self.loc_headers = os.path.join(self.program, "locs/")

        parser = SafeConfigParser({"database": os.path.join(os.getcwdu(), "database/"),
                                   "galaxy": None, "len":None, "locs":None})

        # Open the file with the correct encoding
        with codecs.open(os.path.join(self.program, 'config.ini'), 'r',
                         encoding='utf-8') as f:
            parser.readfp(f)

        d = "Directories"  # the .ini section this class uses.

        # The place where the genome sequences and indexes are stored.
        self.database = parser.get(d, "database")
        self.galaxy = parser.get(d, "galaxy") or self.database

        # # Those can be directed to inside your galaxy instalation,
        # # But for safety, the default is inside the database directory, and
        # # you can move it latter.

        # Move to "/../galaxy/tool-data/shared/ucsc/chrom/"
        self.len = parser.get(d, "len") or os.path.join(self.database, "tool-data/shared/ucsc/chrom/")

        # Where the location files are stored inside your galaxy:
        # at "/../galaxy/tool-data/"
        self.locs = parser.get(d, "locs") or os.path.join(self.database, "tool-data/")




class InfoFile(object):
    '''
    Path to files used by the program.
    '''
    def __init__(self):
        dirs = InfoDir()
        self.sqldb = os.path.join(dirs.database, "sqlite.db")


class Info(object):
    ''' Dummy object where information will be added as atributes dinamically.

    It functions like a dictionary, but with a nicier syntax for acessing the
    values.
    '''
    permissions = 0775  # permmission for directories created.

    dir = InfoDir()
    file = InfoFile()

    def define_genome(self, genome):
        self.unique_build_id = genome.build_id
        self.db_key = genome.db_key
        self.display_name = genome.display_name

    def get_genome(self):
        return Genome(self.unique_build_id, self.db_key, self.display_name)


class Genome(object):
    def __init__ (self, build_id, db_key=None, display_name=None, fasta_file=None):
        self.build_id = build_id
        self.db_key = db_key or self.build_id
        self.display_name = display_name or self.build_id
        dirs = InfoDir()
        self.dir = os.path.join(dirs.database, self.build_id)
        self.fasta_file = fasta_file or os.path.join(self.dir,
                                                   self.build_id + ".fa")

class GenomesToolData():
    def __init__(self, genome1, tool, success=None, date=None, log=None,
                 base_dir=None, file_name=None, base_name_path=None, genome2=None):
        # TODO: On the right are the names on the DB. They need to be harmonized.
        self.genome1 = genome1
        self.genome2 = genome2
        self.tool = tool
        self.success = success
        self.date = date
        self.log = log
        self.dir = base_dir  # differ
        self.file_name = file_name  # file_path is better
        self.base_name_path = base_name_path


    def attribute_iterator(self):
        for attr, value in self.__dict__.items():
            if not callable(value) and not attr.startswith("_"):
                yield attr, value





