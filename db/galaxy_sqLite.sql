CREATE TABLE Genomes (
  genome_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  build_id VARCHAR(250) UNIQUE NOT NULL ,
  db_key VARCHAR(250) NULL ,
  display_name VARCHAR(250) NULL,
  fasta_file  VARCHAR(250) NOT NULL
  );


CREATE  TABLE LocData (
  loc_data_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  file_name VARCHAR(250) NOT NULL ,
  header TEXT NULL
  );
CREATE INDEX index_file_name ON LocData (file_name ASC);

  
CREATE  TABLE Tools (
  tool_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  name VARCHAR(250) NULL ,
  display_name  VARCHAR(250) NULL ,
  alignment  boolean(1) NULL
  );

  
CREATE TABLE Strings (
  loc_data_id INTEGER NOT NULL ,
  tool_id INTEGER NOT NULL ,
  string INTEGER NOT NULL ,
  PRIMARY KEY (loc_data_id, tool_id),
  FOREIGN KEY (loc_data_id) REFERENCES LocData (loc_data_id) ON UPDATE CASCADE,
  FOREIGN KEY (tool_id) REFERENCES Tools (tool_id) ON UPDATE CASCADE
  );

 
CREATE TABLE GenomesToolData (
  genomes_tool_data_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  genome_id_1 INTEGER NOT NULL ,
  genome_id_2 INTEGER NULL ,
  tool_id INTEGER NOT NULL ,
   success  boolean NOT NULL ,
   date  DATETIME NULL ,
   log  TEXT NULL ,
   base_dir  VARCHAR(250) NULL ,
   file_name  VARCHAR(250) NULL ,
   base_name_path  VARCHAR(250) NULL ,
  FOREIGN KEY (genome_id_1) REFERENCES Genomes (genome_id) ON UPDATE CASCADE,
  FOREIGN KEY (genome_id_2) REFERENCES Genomes (genome_id) ON UPDATE CASCADE,
  FOREIGN KEY (tool_id) REFERENCES Tools (tool_id) ON UPDATE CASCADE
  );
