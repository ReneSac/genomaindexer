CREATE  TABLE Genomes (
  genome_id SERIAL ,
  build_id VARCHAR(250) NOT NULL ,
  db_key VARCHAR(250) NULL ,
  display_name VARCHAR(250) NULL ,
  fasta_file VARCHAR(250) NOT NULL ,
  PRIMARY KEY (genome_id) 
  );
CREATE UNIQUE INDEX build_id_UNIQUE ON Genomes (build_id ASC) ;


CREATE  TABLE LocData (
  loc_data_id SERIAL ,
  file_name VARCHAR(250) NOT NULL ,
  header TEXT NULL ,
  PRIMARY KEY (loc_data_id) 
  );
CREATE INDEX file_name_INDEX ON LocData (file_name ASC) ;


CREATE  TABLE Tools (
  tool_id SERIAL,
  name VARCHAR(250) NULL ,
  display_name VARCHAR(250) NULL ,
  alignment BOOLEAN DEFAULT 'f',
  PRIMARY KEY (tool_id) 
  );


CREATE  TABLE Strings (
  loc_data_id INT NOT NULL ,
  tool_id INT NOT NULL ,
  string INT NOT NULL ,
  PRIMARY KEY (loc_data_id, tool_id)
    );
ALTER TABLE Strings ADD CONSTRAINT fk_loc_data_id FOREIGN KEY (loc_data_id) REFERENCES LocData (loc_data_id) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE Strings ADD CONSTRAINT fk_tool_id FOREIGN KEY (tool_id) REFERENCES Tools (tool_id) ON DELETE NO ACTION ON UPDATE CASCADE;

    
CREATE  TABLE GenomesToolData (
  genomes_tool_data_id SERIAL,
  genome_id_1 INT NOT NULL ,
  genome_id_2 INT NULL ,
  tool_id INT NOT NULL ,
  success BOOLEAN DEFAULT 'f',
  date DATE NULL ,
  log TEXT NULL ,
  base_dir VARCHAR(250) NULL ,
  file_name VARCHAR(250) NULL ,
  base_name_path VARCHAR(250) NULL ,
  PRIMARY KEY (genomes_tool_data_id)
    );
ALTER TABLE GenomesToolData ADD CONSTRAINT fk_GTD_Tool1 FOREIGN KEY (tool_id ) REFERENCES Tools (tool_id ) ON DELETE NO ACTION ON UPDATE CASCADE; 
ALTER TABLE GenomesToolData ADD CONSTRAINT fk_GTD_Genome_id_1 FOREIGN KEY (genome_id_1 ) REFERENCES Genomes (genome_id ) ON DELETE NO ACTION ON UPDATE CASCADE;
ALTER TABLE GenomesToolData ADD CONSTRAINT fk_GTD_Genomes_id_2 FOREIGN KEY (genome_id_2 ) REFERENCES Genomes (genome_id ) ON DELETE NO ACTION ON UPDATE CASCADE;