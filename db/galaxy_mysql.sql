SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Genomes`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Genomes` (
  `genome_id` INT NOT NULL AUTO_INCREMENT ,
  `build_id` VARCHAR(250) NOT NULL ,
  `db_key` VARCHAR(250) NULL ,
  `display_name` VARCHAR(250) NULL ,
  `fasta_file` VARCHAR(250) NOT NULL ,
  PRIMARY KEY (`genome_id`) )
ENGINE = InnoDB;

CREATE UNIQUE INDEX `build_id_UNIQUE` ON `mydb`.`Genomes` (`build_id` ASC) ;

CREATE INDEX `build_id_INDEX` ON `mydb`.`Genomes` (`build_id` ASC) ;


-- -----------------------------------------------------
-- Table `mydb`.`LocData`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`LocData` (
  `loc_data_id` INT NOT NULL AUTO_INCREMENT ,
  `file_name` VARCHAR(250) NOT NULL ,
  `header` TEXT NULL ,
  PRIMARY KEY (`loc_data_id`) )
ENGINE = InnoDB;

CREATE INDEX `file_name_INDEX` ON `mydb`.`LocData` () ;


-- -----------------------------------------------------
-- Table `mydb`.`Tools`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Tools` (
  `tool_id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(250) NULL ,
  `display_name` VARCHAR(250) NULL ,
  `alignment` TINYINT(1) NULL ,
  PRIMARY KEY (`tool_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Strings`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Strings` (
  `loc_data_id` INT NOT NULL ,
  `tool_id` INT NOT NULL ,
  `string` INT NOT NULL ,
  PRIMARY KEY (`loc_data_id`, `tool_id`) ,
  CONSTRAINT `fk_loc_data_id`
    FOREIGN KEY (`loc_data_id` )
    REFERENCES `mydb`.`LocData` (`loc_data_id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_tool_id`
    FOREIGN KEY (`tool_id` )
    REFERENCES `mydb`.`Tools` (`tool_id` )
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`GenomesToolData`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`GenomesToolData` (
  `genomes_tool_data_id` INT NOT NULL AUTO_INCREMENT ,
  `genome_id_1` INT NOT NULL ,
  `genome_id_2` INT NULL ,
  `tool_id` INT NOT NULL ,
  `success` TINYINT(1) NULL ,
  `date` DATETIME NULL ,
  `log` TEXT NULL ,
  `base_dir` VARCHAR(250) NULL ,
  `file_name` VARCHAR(250) NULL ,
  `base_name_path` VARCHAR(250) NULL ,
  PRIMARY KEY (`genomes_tool_data_id`) ,
  CONSTRAINT `fk_GenomesToolData_Tool1`
    FOREIGN KEY (`tool_id` )
    REFERENCES `mydb`.`Tools` (`tool_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_GenomesToolData_Genome_id_1`
    FOREIGN KEY (`genome_id_1` )
    REFERENCES `mydb`.`Genomes` (`genome_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_GenomesToolData_Genomes_id_2`
    FOREIGN KEY (`genome_id_2` )
    REFERENCES `mydb`.`Genomes` (`genome_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
