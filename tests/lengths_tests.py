'''
Created on 25/01/2013

@author: rene
'''
import unittest
import build_genoma_db
import os
DATA_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data'))

class Test(unittest.TestCase):


    def testSmallValidFasta(self):
        fname = os.path.join(DATA_DIR, "valid.fasta")
        build_genoma_db.is_NCBI_fasta_DNA(fname)
        self.assertTrue(build_genoma_db.is_NCBI_fasta_DNA(fname))


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.test']
    unittest.main()
