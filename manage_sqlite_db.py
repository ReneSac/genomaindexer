#!/usr/bin/env python
# encoding: utf-8
'''
create_sqlite_db -- shortdesc

create_sqlite_db is a description

It defines classes_and_methods

@author:     user_name

@copyright:  2013 organization_name. All rights reserved.

@license:    license

@contact:    user_email
@deffield    updated: Updated
'''

import config
import sqlite3
import sys
import os
import itertools
import plugin_manager
import locs

__all__ = []
__version__ = 0.1
__date__ = '2013-02-21'
__updated__ = '2013-02-21'

DEBUG = 1
TESTRUN = 0
PROFILE = 0


class SqlDB(object):
    '''

    '''
    def __init__(self, sql_file):
        self.conn = sqlite3.connect(sql_file)


    def add_genome(self, unique_build_id, db_key, display_name, fasta_file):
        '''
        Add a genome to the db if it is not there yet.
        '''
        c = self.conn.cursor()
        try:
            c.execute('SELECT count(*) FROM Genomes WHERE build_id = ?',
                      (unique_build_id,))
            if not c.fetchone()[0]:
                c.execute('INSERT INTO Genomes VALUES(null, ?, ?, ?, ?)',
                          (unique_build_id, db_key, display_name, fasta_file))
                self.conn.commit()
        finally:
            c.close()


    def add_genomeToolData(self, data):
        '''
        Fills many
        Receives a list of tuples, each containing:
        (Genome_unique_build_id, Tool_name, log, base_dir, file_name)

        '''
        c = self.conn.cursor()
        try:
            for item in data:
                # Replace strings by the respective IDs.
                genome_id = c.execute('SELECT Genomes.genome_id FROM Genomes WHERE build_id = ?', (item.genome1,)).fetchone()[0]
                tool_id = c.execute('SELECT Tools.tool_id FROM Tools WHERE name = ?', (item.tool,)).fetchone()[0]
                if item.success is None:
                    print "Sucess is none in: ", item.tool
                    item.success = False
                entry = (genome_id, tool_id, item.success, item.date, item.log, item.dir, item.file_name, item.base_name_path)

                c.execute('INSERT INTO GenomesToolData VALUES (null, ?, null, ?, ?, ?, ?, ?, ?, ?)', entry)

            self.conn.commit()
        finally:
            c.close()

    def get_ToolData(self, tool):
        c = self.conn.cursor()
        try:
            tool_id = c.execute('SELECT Tools.tool_id FROM Tools WHERE name = ?', (tool,)).fetchone()[0]
            c.execute('SELECT * FROM GenomesToolData WHERE tool_id = ?', (tool_id,))
            tooldata = c.fetchall()
            out = []
            for line in tooldata:
                if not line[4]:  # Was not sucessfully built.
                    continue
                genome_id = line[1]
                c.execute('SELECT build_id, db_key, display_name FROM Genomes WHERE genome_id = ?', (genome_id,))
                g = c.fetchone()
                entry = dict(build_id=g[0], db_key=g[1], display_name=g[2], dir=line[7],
                                base_name_path=line[9], file_path=line[8])
                out.append(entry)
            return out
        finally:
            c.close()


    def update_supported_tools(self):
        '''
        Clears the Tools table, and populates it again based on which plugins
        are avaiable on the plugin folder.
        '''

        c = self.conn.cursor()
        try:
            c.execute('DELETE FROM Tools')  # clear the table.
            plugin_list = plugin_manager.list_plugins()
            # FIX-ME: Make plugin_list a list of tuples.

            for name in plugin_list:
                c.execute('INSERT INTO Tools VALUES(null, ?, ?, ?)', (name, name, False))


            self.conn.commit()
        finally:
            c.close()


    def update_loc_info(self):
        c = self.conn.cursor()
        try:
            # clear the tables.
            c.execute('DELETE FROM LocData')
            c.execute('DELETE FROM Strings')

            loc_list = locs.get_locs()

            for loc in loc_list:
                c.execute('INSERT INTO LocData VALUES(null, ?, ?)',
                          (loc.file_name, loc.header))
                query = 'SELECT LocData.loc_data_id FROM LocData WHERE file_name = ?'
                loc_data_id = c.execute(query, (loc.file_name,)).fetchone()[0]
                for string in loc.strings:
                    query = 'SELECT Tools.tool_id FROM Tools WHERE name = ?'
                    tool_id = c.execute(query, (string.tool,)).fetchone()[0]
                    c.execute('INSERT INTO Strings VALUES(?, ?, ?)',
                          (loc_data_id, tool_id, string.string))

            self.conn.commit()
        finally:
            c.close()

    def loc_iterator(self):
        ''' Unfinished '''

        self.conn.row_factory = sqlite3.Row
        c = self.conn.cursor()
        try:
            for loc in c.execute('SELECT * FROM LocData'):
                loc = dict(itertools.izip(loc.keys(), loc))
                # loc['strings'] =

        finally:
            c.close()

    def create_db(self):
        '''
        Creates the sqlite database from the built-in schema.
        '''
        c = self.conn.cursor()
        with open(os.path.join(os.path.dirname(__file__),
                               'db/galaxy_sqLite.sql'), 'r') as sql:
            c.executescript(sql.read())
        self.conn.commit()
        c.close()



def main(argv=None):
    '''Command line options.'''
    info = config.Info()

    db = SqlDB(info.file.sqldb)

    db.create_db()
    db.update_supported_tools()

    pass




if __name__ == "__main__":
    if DEBUG:
        sys.argv.append("-h")
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'create_sqlite_db_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())
