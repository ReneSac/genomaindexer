﻿  #!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  Plugin.
#
#  Copyright (c) 2012 René du R. Sacramento
#  Released under the MIT license.


from .count_fasta import lengths
import os
import shutil
import util


subdir = __name__
run_first = []


def run(info, exec_info):
    ''' Calculates the length of DNA sequences from a fasta file and write
    as a .len file, with tab separator. Used by the Trackster.'''
    fname = info.unique_build_id + ".len"
    lengths(info.ref_fasta, fname)
    util.makedirs_if_absent(info.permissions, info.dir.len)
    shutil.copy(fname, info.dir.len)
    exec_info.log = "Done."
    exec_info.dir = os.path.join(os.getcwd(), '')
    exec_info.file_path = os.path.join(exec_info.dir, fname)
    return exec_info
