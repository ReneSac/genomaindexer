#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  Program to calculate the length of DNA sequences from a fasta file and write
#  as a .len file, with tab separator.
#
#  Copyright (c) 2012 René du R. Sacramento
#  Released under the MIT license.

import re


def lengths(ref_fasta, out_name):
    ''' Calculates the length of DNA sequences from a fasta file and write
    as a .len file, with tab separator. Used by the Trackster.'''
    counts = {}
    with open(ref_fasta, "r") as in_fasta:
        seq_name = None
        for line in in_fasta:  # regex is a bit slow, so do a basic find first:
            if line[:1] == ">":  # about 2 times faster than: line.startswith(">"):
                m = re.match(r"\>(\S+)", line)
                seq_name = m.group(1)
                counts[seq_name] = 0
                print "Counting sequence", seq_name, '...'
            else:
                counts[seq_name] += len(line.strip())  # do not count white space.

    # "wb" guarantees POSIX line-breaks ('\n') even on Windows, etc.
    with open(out_name, "wb") as output:
        for k, v in sorted(counts.items(), key=lambda (k, v): v, reverse=True):
            output.write(k + "\t" + str(v) + "\n")

    average = sum(counts.values()) / len(counts)
    print '\nAverage size:', average, "bp per sequence."


if __name__ == "__main__":
    from optparse import OptionParser

    usage = "usage: %prog fasta_file output_name"
    parser = OptionParser(usage)
    (options, args) = parser.parse_args()

    if len(args) != 2:
        parser.error("Incorrect number of arguments.")

    lengths(args[0], args[1])
