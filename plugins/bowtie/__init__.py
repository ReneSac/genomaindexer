﻿  #!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  Plugin.
#
#  Copyright (c) 2012 René du R. Sacramento
#  Released under the MIT license.

from subprocess import STDOUT
from util import check_output  # Backport from python 2.7
import os

subdir = __name__
run_first = []

def run(info, exec_info):
    ''' Bowtie index for TopHat, etc. Takes more than 1 hour to build.'''
    log = check_output(["bowtie-build", info.ref_fasta, info.unique_build_id],
                     stderr=STDOUT)
    exec_info.log = log
    exec_info.dir = os.path.join(os.getcwd(), '')
    exec_info.base_name_path = os.path.join(exec_info.dir, info.unique_build_id)
    return exec_info
