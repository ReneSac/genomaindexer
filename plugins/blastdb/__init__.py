﻿  #!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  Plugin.
#
#  Copyright (c) 2012 René du R. Sacramento
#  Released under the MIT license.

from subprocess import STDOUT
from util import check_output  # Backport from python 2.7
import os

subdir = __name__
run_first = []

def run(info, exec_info):
    '''Builds database for Blast work.'''
    os.symlink(info.ref_fasta, info.fasta)  # Makes it build in this dir.
    log = check_output(["formatdb", "-i", info.fasta, "-o", "F", "-p", "F"],
                     stderr=STDOUT)
    exec_info.log = log
    exec_info.dir = os.path.join(os.getcwd(), '')
    exec_info.base_name_path = os.path.join(exec_info.dir, info.fasta)
    return exec_info
