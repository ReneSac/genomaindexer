﻿  #!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  Plugin.
#
#  Copyright (c) 2012 René du R. Sacramento
#  Released under the MIT license.

from subprocess import STDOUT
from util import check_output  # Backport from python 2.7
import os

subdir = __name__
run_first = []

def run(info, exec_info):
    '''.2bit for lastz_seqs'''
    file_name = info.unique_build_id + ".2bit"
    log = check_output(["faToTwoBit", info.ref_fasta, file_name], stderr=STDOUT)
    exec_info.log = log
    exec_info.dir = os.path.join(os.getcwd(), '')
    exec_info.file_path = os.path.join(exec_info.dir, file_name)
    return exec_info
