﻿  #!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  Plugin.
#
#  Copyright (c) 2012 René du R. Sacramento
#  Released under the MIT license.

from subprocess import STDOUT
from util import check_output  # Backport from python 2.7
import os

subdir = __name__
run_first = []

def run(info, exec_info):
    '''Picard's .dict and .fai files.'''
    picard_exec = os.path.join(info.dir.galaxy, "tool-data/shared/jars/picard/CreateSequenceDictionary.jar")
    os.symlink(info.ref_fasta, info.fasta)
    log1 = check_output(["java", "-jar", picard_exec, "R=" + info.fasta,
                "O=" + info.unique_build_id + ".dict"], stderr=STDOUT)
    log2 = check_output(["samtools", "faidx", info.fasta], stderr=STDOUT)
    exec_info.log = log1 + log2
    exec_info.dir = os.path.join(os.getcwd(), '')
    exec_info.base_name_path = os.path.join(exec_info.dir, info.fasta)
    return exec_info
