﻿  #!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  Plugin.
#
#  Copyright (c) 2012 René du R. Sacramento
#  Released under the MIT license.

from subprocess import STDOUT
from util import check_output  # Backport from python 2.7
import os
from glob import iglob

subdir = __name__
run_first = ['fa']

def run(info, exec_info):
    '''Files .nib'''
    log = ""
    for fname in iglob('../fa/*.fa'):
        seq_name, _ = os.path.splitext(os.path.basename(fname))
        log += check_output(['faToNib', fname, seq_name + '.nib'],
                     stderr=STDOUT)
    exec_info.log = log
    exec_info.dir = os.path.join(os.getcwd(), '')
    # exec_info.base_name_path = os.path.join(exec_info.dir, info.unique_build_id)
    return exec_info
