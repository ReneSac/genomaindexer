﻿  #!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  Plugin.
#
#  Copyright (c) 2012 René du R. Sacramento
#  Released under the MIT license.

from .cut_fasta import fa
import os

subdir = __name__
run_first = []

def run(info, exec_info):
    '''Read DNA sequences and save them as individual files.'''
    fa(info.ref_fasta)
    exec_info.log = "Done."
    exec_info.dir = os.path.join(os.getcwd(), '')
    return exec_info
