﻿  #!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  Reads DNA sequences from a fasta file and save them as individual files.
#
#  Copyright (c) 2012 René du R. Sacramento
#  Released under the MIT license.

import re

def fa(ref_fasta):
    in_fasta = open(ref_fasta, "r")
    output = None

    for line in in_fasta:  # regex is a bit slow, so do a basic find first:
        if line[:1] == ">":  # about 2 times faster than: line.startswith(">")
            m = re.match(r"\>(\S+)", line)
            seq_name = m.group(1)
            print "Writting sequence", seq_name, "..."
            if output:
                output.close()
            output = open(seq_name + ".fa", "wb")
        output.write(line)  # A buffer uses lots of memory and isn't much faster.

    in_fasta.close()
    output.close()


if __name__ == "__main__":
    from optparse import OptionParser

    usage = "usage: %prog fasta_file"
    parser = OptionParser(usage)
    (options, args) = parser.parse_args()

    if len(args) != 1:
        parser.error("Incorrect number of arguments.")

    fa(args[0])
