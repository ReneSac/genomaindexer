﻿  #!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  Plugin.
#
#  Copyright (c) 2012 René du R. Sacramento
#  Released under the MIT license.

import os

subdir = None
run_first = []

def run(info, exec_info):
    ''' Do nothing.

    This was already done by the work_dir_setup() in build_genome_db as
    it is essential for everything.
    '''
    exec_info.log = "Done."
    exec_info.dir = info.dir.work
    exec_info.file_name = os.path.join(exec_info.dir, info.fasta)
    exec_info.base_name_path = os.path.join(exec_info.dir, info.unique_build_id)
    return exec_info
