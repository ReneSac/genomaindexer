#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
  Generates updated location files (.loc)

  Copyright (c) 2012 René du R. Sacramento
  Released under the MIT license.
'''

import config
import manage_sqlite_db
import os
import util

class Loc(object):
    def __init__ (self, file_name, header, strings):
        self.file_name = file_name
        self.header = header
        self.strings = strings


class LocString(object):
    def __init__(self, tool, string):
        self.tool = tool
        self.string = string

class GenLoc(object):
    '''
    Generate/updates the location files (.loc)

    When updating, the old files are deleted and new files written based on the
    database information.
    '''

    def __init__(self):
        '''
        Nothing to do here
        '''
        pass


    def update_all(self, info, built):
        '''
        Call update_loc() for all locs that need to be updated.

        Won't bother updating locs for tools whose indexes weren't built.
        '''
        db = get_locs()  # FIX-ME: do really acess the DB.
        for loc in db:
            for string in loc.strings:
                if string.tool in built:
                    self.update_loc(loc, info, built)
                    break  # All strings for this loc were generated.


    def update_loc(self, loc, info, built):
        '''
        Writes a single loc file, with any number of string types and lines.
        '''
        out = []
        db = manage_sqlite_db.SqlDB(info.file.sqldb)
        for string in loc.strings:
            fstring = string.string
            genomes = db.get_ToolData(string.tool)
            for genome in genomes:
                line = fstring
                for field, data in genome.items():
                    if data:
                        line = line.replace("<" + field + ">", data)
                out.append(line)

        util.makedirs_if_absent(info.permissions, info.dir.locs)
        loc_file = os.path.join(info.dir.locs, loc.file_name)
        util.silent_backup(loc_file)

        with open(loc_file, "wb") as out_file:
            out_file.write(loc.header)
            for line in out:
                out_file.write(line)
                out_file.write("\n")


# TODO: Initial configuration info. Move somewhere else.
proto_locs = [
    Loc('alignseq.loc', '', [LocString('seq', "seq\t<build_id>\t<dir>")]),
    Loc('all_fasta.loc', '', [LocString('multifasta', '<build_id>\t<db_key>\t<display_name>\t<file_path>')]),
    Loc('blastdb.loc', '', [LocString('blastdb', '<build_id>\t<display_name>\t<base_name_path>')]),
    Loc("bowtie2_indices.loc", '', [LocString('bowtie2', '<build_id>\t<db_key>\t<display_name>\t<base_name_path>')]),
    Loc('bowtie_indices.loc', '', [LocString('bowtie', '<build_id>\t<db_key>\t<display_name>\t<base_name_path>')]),
    Loc('faseq.loc', '', [LocString('fa', '<build_id>\t<dir>')]),
    Loc('lastz_seqs.loc', '', [LocString('twobit', '<build_id>\t<display_name>\t<file_path>')]),
    Loc('picard_index.loc', '', [LocString('picard', '<build_id>\t<db_key>\t<display_name>\t<dir>')]),  # It actually tells us to use the <base_name_path> <-- the header is wrong...
    Loc('sam_fa_indices.loc', '', [LocString('picard', 'index\t<build_id>\t<base_name_path>')]),
    Loc('srma_index.loc', '', [LocString('picard', '<build_id>\t<db_key>\t<display_name>\t<base_name_path>')]),
    ]

def get_locs():
    '''
    Makes a list of full .loc descriptor objects to initially populate the
    database.
    '''
    info = config.Info()
    locs = []
    for loc in proto_locs:
        header_file = os.path.join(info.dir.loc_headers, loc.file_name)
        loc.header = open(header_file, "rb").read()
        locs.append(loc)
    return locs

