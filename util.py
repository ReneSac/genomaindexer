#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
 Utility functions that aren't directly provided by python itself.
'''

import os
import errno
import subprocess

# Our exeptions:

class FastaError(Exception):
    pass

class ToolError(Exception):
    pass


# Utility functions

def makedirs_if_absent(permissions, *dirs):
    for dirname in dirs:
        try:
            os.makedirs(dirname, permissions)
        except OSError as e:
            if e.errno != errno.EEXIST:  # EEXIST = directory already exists
                raise  # re-raise exception if a different error occured


def clean_up_work_dir(work_dir, *dest):
    try:
        silent_remove(dest)
        os.rmdir(work_dir)
    except (IOError, OSError) as e:
        print "Post-error clean up aborted:", str(e)


def silent_remove(*files):
    for filename in files:
        try:
            os.remove(filename)
        except OSError as e:
            if e.errno != errno.ENOENT:  # ENOENT = no such file or directory
                raise  # re-raise exception if a different error occured

def silent_backup(*files):
    for filepath in files:
        try:
            os.rename(filepath, filepath + '.bak')  # .orig may be confusing.
        except OSError as e:
            if e.errno != errno.ENOENT:  # ENOENT = no such file or directory
                raise


# Backport for python 2.6.
# Copied from directly from python 3.2 stdlib, with some changes.
def check_output(*popenargs, **kwargs):
    r"""Run command with arguments and return its output as a byte string.

    If the exit code was non-zero it raises a CalledProcessError.  The
    CalledProcessError object will have the return code in the returncode
    attribute and output in the output attribute.

    The arguments are the same as for the Popen constructor.  Example:

    >>> check_output(["ls", "-l", "/dev/null"])
    b'crw-rw-rw- 1 root root 1, 3 Oct 18  2007 /dev/null\n'

    The stdout argument is not allowed as it is used internally.
    To capture standard error in the result, use stderr=STDOUT.

    >>> check_output(["/bin/sh", "-c",
    ...               "ls -l non_existent_file ; exit 0"],
    ...              stderr=STDOUT)
    b'ls: non_existent_file: No such file or directory\n'
    """
    if 'stdout' in kwargs:
        raise ValueError('stdout argument not allowed, it will be overridden.')
    process = subprocess.Popen(*popenargs, stdout=subprocess.PIPE, **kwargs)
    output, unused_err = process.communicate()
    retcode = process.poll()
    if retcode:
        cmd = kwargs.get("args")
        if cmd is None:
            cmd = popenargs[0]
        err = subprocess.CalledProcessError(retcode, cmd)
        err.output = output
        raise err
    return output


def is_NCBI_fasta_DNA(fname):
    '''
        This function tests only the two first lines of the file.

        A fasta file, acording to the NCBI specification, must start with a >
        followed imediatedly by a sequence name and then a sequence. ';'
        comments and blank lines are not tools.
    '''
    error = ("Invalid genome file provided: %s Only files following " +
                            "NCBI fasta specification are accepted.")
    valid_dna = ("A", "T", "C", "G", "N", "a", "t", "c", "g", "n", "-")
    with open(fname) as f:
        line = f.readline()

        if not line.startswith(">"):
            raise FastaError(error % 'The first character isnt ">".')
        if line[1].isspace():
            raise FastaError(error % 'Must have a valid sequence identifier, not a space, after ">".')

        line2 = f.readline().rstrip("\r\n")

        if not line2:
            raise FastaError(error % "Empty second line. Expected DNA sequence.")
        if len(line2) > 80:
            raise FastaError(error % "DNA sequence line too long. Please hard-wrap it at a no more than 80 characters per line.")
        for char in line2:
            if char not in valid_dna:
                raise FastaError(error % "Strange character found in the DNA sequence.")

    return True

