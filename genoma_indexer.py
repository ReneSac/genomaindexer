#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  Builds all the databases needed by Galaxy from a fasta genome file.
#
#  Copyright (c) 2012 René du R. Sacramento
#  Released under the MIT license.


import config
import plugin_manager
import manage_sqlite_db
import argparse
from subprocess import CalledProcessError
from multiprocessing import Process, Queue
from datetime import datetime
import shutil
import locs
import os
import util



# Global variables


tools, built = {}, {}


# Main control functions:

def work_dir_setup(input_genome, info):
    '''
    Creates base main folder and the unique_build_id.fa file.

    Also creates the sqldb if absent.
    '''
    dest = info.ref_fasta
    work_dir = info.dir.work
    util.makedirs_if_absent(info.permissions, info.dir.database, work_dir,
                           os.path.dirname(info.file.sqldb))

    print "Work_dir:", work_dir

    if not os.path.isfile(info.file.sqldb):
        db = manage_sqlite_db.SqlDB(info.file.sqldb)
        db.create_db()
        db.update_supported_tools()
    try:
        util.is_NCBI_fasta_DNA(input_genome)
        shutil.copy(input_genome, dest)
        os.chdir(work_dir)
    except Exception:
        util.clean_up_work_dir(work_dir, dest)
        raise
    else:
        db = manage_sqlite_db.SqlDB(info.file.sqldb)
        db.add_genome(info.unique_build_id, info.db_key, info.display_name, dest)



def generate_dbs(ops, info, abort=False):
    q = Queue()
    t = {}
    for op in ops:
        # TODO: If an op has no dependencies, paralelize.
        ok, missing = check_dependencies(op)
        if not ok:
            built[op].success = False
            built[op].log = "Skipping %s because the following dependencies were not successfully built: %s" % (op, missing)
            print "\n", built[op].log
            continue
        print "\nBuilding: ", op, "..."
        t[op] = Process(target=build_db, args=(tools[op], info, built[op], q))
        t[op].start()
        built[op] = q.get()
        t[op].join()

        if abort and built[op].success == False:
            raise util.ToolError("Couldn't build database %s" % op)


def check_dependencies(op):
    ok = True
    missing = []
    for dependencie in tools[op].run_first:
        if not built[dependencie].success:
            ok = False
            missing.append(dependencie)
    return ok, missing


def build_db(tool, info, exec_info, q):
    ''' Should not be called directly. Use generate_dbs(). '''
    cwd = os.getcwd()
    os.chdir(info.dir.work)
    try:
        if tool.subdir:  # multifasta is the only where this is false.
            util.makedirs_if_absent(info.permissions, tool.subdir)
            os.chdir(tool.subdir)
        exec_info = tool.run(info, exec_info)  # actually execute.
    except (CalledProcessError, ValueError, OSError, IOError,
            util.ToolError) as e:
        error_str = "Error building %s database: %s" % (exec_info.tool, str(e))
        if hasattr(e, 'child_traceback'):
            error_str += "\nChild traceback: \n" + str(e.child_traceback)
        print error_str
        exec_info.success = False
        exec_info.log = error_str
    else:
        print "\nBuilt!", exec_info.tool
        exec_info.success = True
    finally:
        exec_info.date = datetime.utcnow()
        q.put(exec_info)
        os.chdir(cwd)


def reorder_ops(ops, tools):
    '''
    Order the operations so that every dependency is built before the tool
    that requires it.
    '''
    ordered = []
    for op in ops:
        dependencies = reorder_ops(tools[op].run_first, tools)
        for dependencie in dependencies:
            if dependencie not in ops:
                ordered.append(dependencie)
        ordered.append(op)
    return ordered


def build_genome_db(genome_file, info, ops):
    '''
    Given the information, executes all the operations asked, plus calculates
    the length (and will generate the respective loc files when finished).
    '''
    # Derived atributes
    info.dir.work = os.path.join(info.dir.database, info.unique_build_id + '/')
    info.fasta = info.unique_build_id + ".fa"
    info.ref_fasta = os.path.join(info.dir.work, info.fasta)

    # Do things.
    global tools, built
    if genome_file:  # First time indexing this genoma
        ops.append("multifasta")

    tools = plugin_manager.load_plugins(ops)
    ops = reorder_ops(ops, tools)

    for op in tools:  # FIX-ME: check for empty ops
        if op not in built:
            built[op] = config.GenomesToolData(info.unique_build_id, op)

    if genome_file:
        work_dir_setup(genome_file, info)

    # Run the indexers
    generate_dbs(ops, info)

    # Update the database
    assert set(ops).issubset(built)
    db = manage_sqlite_db.SqlDB(info.file.sqldb)
    g = [value for _, value in built.iteritems()]
    db.add_genomeToolData(g)
    db.update_loc_info()

    # Update the location files.
    genloc = locs.GenLoc()
    genloc.update_all(info, built)


def main():
    ''' Parses the command line arguments and calls build_genome_db(). '''
    parser = argparse.ArgumentParser(description="""Builds all the databases
            needed by Galaxy from a fasta genome file.""")
    parser.add_argument("unique_build_id", metavar="unique-build-id",
                        help="""Unique name for this specific
                        version of this specific genome. Should be short and
                        cannot have spaces or special characters.""")
    parser.add_argument("-f", "--genome-file", help="Multi fasta genome file.")
    parser.add_argument("-n", "--display-name",
                      help="""Human readable name for display. Recommended.
                      It can have spaces, etc. Defauts to unique_build_id.""")
    parser.add_argument("-k", "--db_key",
                      help="Key for database. Defaults to the unique_build_id.")
    parser.add_argument("-d", "--select-dbs", nargs='+',
                        help="""If used, only the databases listed following it
                        in the command-line will be built.""")
    verbosity = parser.add_mutually_exclusive_group()
    verbosity.add_argument("-v", "--verbose", action="store_true")
    verbosity.add_argument("-q", "--quiet", action="store_false")

    args = parser.parse_args()


    # Fill info table.
    info = config.Info()

    # Atributes based on the command-line.
    info.define_genome(config.Genome(args.unique_build_id,
                                    args.db_key, args.display_name))
    info.vebose = args.verbose or True

    # Do things.

    ops = args.select_dbs or plugin_manager.list_plugins()
    build_genome_db(args.genome_file, info, ops)

    print "All done!"


if __name__ == "__main__":
    main()
