﻿This is a tool written in python to build indexes and generate location files 
(.loc) from multifasta genome files. It is built on top of a plugin 
architecture and stores all the information in a SQLite database.

It is still under development, and may contain bugs.

Future releases will integrate the current tables inside the Galaxy main 
database, and hopefully tools may have direct acess to the location information 
rather than relly on the flat files.

==================
Usage:  
==================

genoma_indexer.py [-h] [-f GENOME_FILE] [-n DISPLAY_NAME] [-k DB_KEY]
                           [-d SELECT_DBS [SELECT_DBS ...]] [-v | -q]
                           unique-build-id

positional arguments:
  unique-build-id       Unique name for this specific version of this specific
                        genome. Should be short and cannot have spaces or
                        special characters.

optional arguments:
  -h, --help            show this help message and exit
  -f GENOME_FILE, --genome-file GENOME_FILE
                        Multi fasta genome file.
  -n DISPLAY_NAME, --display-name DISPLAY_NAME
                        Human readable name for display. Recommended. It can
                        have spaces, etc. Defauts to unique_build_id.
  -k DB_KEY, --db_key DB_KEY
                        Key for database. Defaults to the unique_build_id.
  -d SELECT_DBS [SELECT_DBS ...], --select-dbs SELECT_DBS [SELECT_DBS ...]
                        If used, only the databases listed following it in the
                        command-line will be built.
                        

___________________
Example comandline:
-------------------

python genoma_indexer.py drosophila69 -d "Drosophila melanogaster BDGP5 69" -f 
Drosophila_melanogaster.BDGP5.69.dna.toplevel.fa -d lengths seq picard blastdb

or simply:
python genoma_indexer.py drosophila69 -f 
Drosophila_melanogaster.BDGP5.69.dna.toplevel.fa

To run all available indexers, w/o a custom human readable name. It defaults to 
the unique-build-id, that must be supplied.

_________________
Output directory:
-----------------
You may chose the directories that will be used by the program, especially
for output, by editing the config.ini file on the program directory. 

By default, the database directory will be created in the same folder where you 
execute the program, and all generated outputs will be placed inside it. Be 
carefull because the original content of those directories may be overwritten.

===================
Rationale:
===================

In order to make new genomes available for users of a given local Galaxy 
instance, the system administrator is required to pre-process each genome 
sequence file. Such file has to be indexed several times in order to fulfill 
the requirements of each analysis tools. The indexed genome files location are 
registered in flat files to enable the tool to locate them. Manually running 
the indexers and updating the locations files is a tedious, error prone 
process. Such index errors are usually only noticed later, when running a tool 
that depends on it, halting an analysis workflow. 

=================
TODO:
=================
- Migrate from sqlite3 module to sqlalchemy, for managing the database.
- Migrate the plugin framework to Yapsi, instead of this custom one.
- Implement progress bar.
- Make the process paralelizable, for servers with dozens of cores and lots of 
memory.
- Fix handling of unicode commandline entries.