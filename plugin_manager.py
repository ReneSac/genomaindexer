#!/usr/bin/env python
# encoding: utf-8

'''
Created on 22/02/2013

@author: rene
'''

# TODO: Use Yapsi instead of this custom plugin manager.

import config
import os
import imp
from glob import iglob

NO, YES, BUILDING, FAILED, UNKNOWN = range(5)  # Poor's man enum

info = config.Info()


def iterate_plugins():
    for plugins_dir in info.dir.plugins:
        for plugin in iglob(os.path.join(plugins_dir, "*", "__init__.py")):
            plugin_path = os.path.dirname(plugin)
            name = os.path.basename(plugin_path)
            yield name, plugin_path

def load_plugin_by_name(name):
    module = None
    for plugins_dir in info.dir.plugins:
        try:
            module = imp.load_module(name, None, os.path.join(plugins_dir, name),
                                                  ("", "", imp.PKG_DIRECTORY))
        except Exception:
            pass

        if module:
            break

    if not module:
        raise Exception("Module %s not found. Make sure you typed the right module name." % name)

    if not hasattr(module, "run_first"):
        raise Exception("Module %s don't have run_first attribute. Make sure you typed the right module name." % name)

    return module


def load_plugins(selected):
    '''
    Fechs all plugins and return as a dictionary.
    It can be safelly called any time to update the list of plugins.
    '''
    tools = {}
    for plugin in selected:
        if plugin not in tools:  # FIX-ME: Check if the plugin was imported, then add to the table.
            tools[plugin] = load_plugin_by_name(plugin)
        dependencies = load_plugins(tools[plugin].run_first)

        for plugin, module in dependencies.iteritems():
            if plugin not in tools:
                tools[plugin] = module
    return tools


def list_plugins():
    tools = []
    for name, _ in iterate_plugins():
        tools.append(name)
    return tools


